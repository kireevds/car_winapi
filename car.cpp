//--------------------------------------------------- 
#include <windows.h>
#include <tchar.h>
#include <stdio.h>
#include <math.h>

LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);
TCHAR WinName[] = _T("MainFrame");

//---------------------------------------------------
int APIENTRY WinMain(HINSTANCE This, HINSTANCE Prev, LPSTR cmd, int mode) {
	HWND hWnd; //десскиптор главного окна программы 
	MSG msg; // структура для храниения сообщений 
	WNDCLASS wc; // Класс окна

	// Определение класса окна
	wc.hInstance = This;
	wc.lpszClassName = WinName; // Имя класса окна
	wc.lpfnWndProc = WndProc; // Функция окна
	wc.style = CS_HREDRAW | CS_VREDRAW; // Стиль окна
	wc.hIcon = LoadIcon(NULL, IDI_APPLICATION); // Стандартная иконка 
	wc.hCursor = LoadCursor(NULL, IDC_ARROW); // Стандартный курсор 
	wc.lpszMenuName = NULL; // Нет меню
	wc.cbClsExtra = 0; // Нет дополнительных данных класса 
	wc.cbWndExtra = 0; // Нет дополнительных данных окна

	// Заполение окна белым цветом
	wc.hbrBackground = (HBRUSH)(COLOR_WINDOW + 1);
	if (!RegisterClass(&wc)) return 0; // Регистрация класса окна

	// Создание окна
	hWnd = CreateWindow(WinName, // Имя класса окна
		_T("Машина"), // Заголовок окна 
		WS_OVERLAPPEDWINDOW, // Стиль окна
		CW_USEDEFAULT, // X
		CW_USEDEFAULT, // Y
		CW_USEDEFAULT, // Width
		CW_USEDEFAULT, // Height
		HWND_DESKTOP, //Дескриптор родительского окна 
		NULL, // Нет меню
		This, // Дескриптор приложения
		NULL); // Дополнительной информации нет
	ShowWindow(hWnd, mode); // Показать окно 

	// Цикл обработки событий 
	while (GetMessage(&msg, NULL, 0, 0))
	{
		TranslateMessage(&msg); // Трансляция кодов нажатой клавиши 
		DispatchMessage(&msg);// Посылка сообщения функции WndProc
	}return 0;
}

double FuncLine(POINT p1, POINT p2, double x, double y){
	return x*(p1.y - p2.y) - y*(p1.x - p2.x) + p1.x*p2.y - p2.x*p1.y;
}
double FuncCircle(POINT p1, double R, double x, double y){
	return R*R - (x - p1.x)*(x - p1.x) - (y - p1.y)*(y - p1.y);
}
double FuncUnion(double w1, double w2){
	return w1 + w2 + sqrt(w1*w1 + w2*w2);
}
double FuncInterselect(double w1, double w2){
	return w1 + w2 - sqrt(w1*w1 + w2*w2);
}

//---------------------------------------------------
// Оконна функция, вызываемая ОС и получает
// сообщения из очереди для данного приложеня
LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	PAINTSTRUCT ps;
	HDC hdc;
	HPEN hPen1 = CreatePen(PS_SOLID, 2, RGB(255, 0, 17));  //цвет фигуры
	HPEN hPen2 = CreatePen(PS_SOLID, 2, RGB(52, 110, 145); //цвет фона
	static int sx, sy;

	// Переменные R-функций
	double w1, w2, w3, w4, w5, w6, w7, w8, w9, w10, w11,
		w12, w13, w14, w15, w16, w17, w18, w19, w20, w21, w;
		//	w22, w23, w24, w25, w26, w27, w28, w29, w30, w31, w;

		// Координаты основных точек
		int n = 16;
	POINT* point = new POINT[n];
	point[0].x = 200;
	point[0].y = 400;

	point[1].x = 800;
	point[1].y = 400;

	point[2].x = 300;
	point[2].y = 400;

	point[3].x = 650;
	point[3].y = 400;

	point[4].x = 800;
	point[4].y = 300;

	point[5].x = 578;
	point[5].y = 150;

	point[6].x = 355;
	point[6].y = 300;

	point[7].x = 570;
	point[7].y = 150;

	point[8].x = 580;
	point[8].y = 260;

	point[9].x = 590;
	point[9].y = 260;

	point[10].x = 578;
	point[10].y = 180;

	point[11].x = 590;
	point[11].y = 180;

	point[12].x = 578;
	point[12].y = 300;

	point[13].x = 480;
	point[13].y = 260;

	point[14].x = 480;
	point[14].y = 250;

	int eps = 1;

	// Обработчик сообщений
	switch (message)
	{
	case WM_CREATE:
		break;
	case WM_SIZE:
		sx = LOWORD(lParam); // Ширина окна
		sy = HIWORD(lParam); // Высота окна
		break;
	case WM_PAINT:
		hdc = BeginPaint(hWnd, &ps);
		for (int x = 0; x<sx; x += 2)
		{
			for (int y = 0; y<sy; y += 2)
			{
				double w1 = FuncLine(point[0], point[1], x, y);
				double w2 = FuncCircle(point[2], 55, x, y);//левая колесная рама
				double w3 = FuncInterselect(-w1, -w2);
				double w4 = FuncCircle(point[3], 55, x, y);//правая колесная рама
				double w5 = FuncInterselect(w3, -w4);
				double w6 = FuncLine(point[1], point[4], x, y);
				double w7 = FuncInterselect(w5, -w6);
				double w8 = FuncCircle(point[2], 50, x, y);//левое колесо
				double w9 = FuncCircle(point[2], 10, x, y);
				double w10 = FuncInterselect(w8, -w9);
				double w11 = FuncUnion(w7, w10);
				double w12 = FuncCircle(point[3], 50, x, y);//правое колесо
				double w13 = FuncCircle(point[3], 10, x, y);
				double w14 = FuncInterselect(w12, -w13);
				double w15 = FuncUnion(w11, w14); //днище с колесами
				double w16 = FuncCircle(point[12], 222, x, y); //крыша
				double w17 = FuncLine(point[5], point[7], x, y);
				double w18 = FuncInterselect(w16, -w17);
				double w19 = FuncCircle(point[2], 150, x, y);//капот
				double w20 = FuncLine(point[8], point[9], x, y);
				double w21 = FuncInterselect(w19, w20);
				double w22 = FuncUnion(w21, w18);
				double w23 = FuncInterselect(w22, w15); //весь кузов

				double w24 = FuncLine(point[10], point[11], x, y); //заднее окно
				double w25 = FuncInterselect(-w20, w24);
				double w26 = FuncLine(point[12], point[10], x, y);
				double w27 = FuncInterselect(w25, w26);
				double w28 = FuncCircle(point[12], 190, x, y);
				double w29 = FuncInterselect(w27, w28);
				double w30 = FuncInterselect(w23, -w29);

				double w31 = FuncCircle(point[13], 81, x, y); //переднее окно
				double w32 = FuncInterselect(-w20, w31);
				double w33 = FuncLine(point[13], point[14], x, y);
				double w34 = FuncInterselect(w32, -w33);

				double w = FuncInterselect(w30, -w34);

				//выбор цвета кисти (фигура-фон)
				if (w > 0) SelectObject(hdc, hPen1);
				else SelectObject(hdc, hPen2);
				Ellipse(hdc, x - eps, y - eps, x + eps, y + eps);
			}
		}
		EndPaint(hWnd, &ps);
		break;
	case WM_DESTROY: PostQuitMessage(0);
		break; // Завершение программы
		// Обработка сообщеня
	default: return DefWindowProc(hWnd, message, wParam, lParam);
	}
	return 0;
}